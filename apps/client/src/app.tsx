import React from 'react';
import { Switch } from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home';
import { Layout } from './layout';

export const App = () => {
  return (
    <Switch>
      <Layout exact path="/" component={Home}/>
      <Layout exact path="/login" component={Login}/>
      <Layout exact path="/signup" component={Login}/>
    </Switch>
  );
};

export default App;

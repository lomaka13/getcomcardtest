import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from 'react-apollo';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { blue, red } from '@material-ui/core/colors';
import { ApolloLink, from, split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { ErrorLink } from 'apollo-link-error';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { getMainDefinition } from 'apollo-utilities';
import { environment } from './environments/environment'

import App from './app';

const wsclient = new SubscriptionClient(environment.SOCKET_URL, {
  reconnect: true,
  connectionParams: {
    authToken: localStorage.getItem('token') ? `Bearer ${localStorage.getItem('token')}` : null
  }
});
const httplink = new HttpLink({ uri: environment.API_URL });
const wslink = new WebSocketLink(wsclient);

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      authorization: localStorage.getItem('token') ? `Bearer ${localStorage.getItem('token')}` : null
    }
  });

  return forward(operation);
});
const errLink = new ErrorLink(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.forEach(({ message, locations, path }) =>
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    );
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const link = from([
  errLink,
  authMiddleware,
  httplink
]);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: split(
    ({ query }) => {
      const definition = getMainDefinition(query);
      return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
    },
    wslink,
    link
  )
});
const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: red
  }
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <ApolloHooksProvider client={client}>
      <BrowserRouter>
        <MuiThemeProvider theme={theme}>
          <App/>
        </MuiThemeProvider>
      </BrowserRouter>
    </ApolloHooksProvider>
  </ApolloProvider>,

  document.getElementById('root')
);

import React from 'react';
import { Route } from 'react-router-dom';
import { getToken } from './helpers';
import Login from './pages/Login';

export const Layout = ({ component: Component, ...history }) => {
  if (!getToken()) {
    return <Route {...history} render={matchProps => <Login {...matchProps} />}/>;
  }

  return (
    <Route
      {...history}
      render={matchProps => {
        return (
          <Component {...matchProps} />
        );
      }}
    />
  );
};

import gql from 'graphql-tag';

export const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(loginInput: {email: $email, password: $password}) {
      jwt
    }
  }
`;

export const SIGNUP = gql`
  mutation signUp($email: String!, $password: String!) {
    signup(signUpInput: {email: $email, password: $password}) {
      jwt
    }
  }
`;

export const USER_PROJECTS = gql`
  query getUserProjects ($id: Int!) {
    user(id: $id) {
      projects {
        createdAt
        forks
        id
        issues
        name
        owner
        stars
        updatedAt
        url
      }
    }
  }

`;

export const GET_PROJECT = gql`
  query getProject ($id: Int!) {
    project(id: $id) {
      id
      url
      name
      owner
      stars
      forks
      issues
      createdAt
      updatedAt
    }
  }
`;

export const CREATE_PROJECT = gql`
  mutation createCroject($url: String!) {
    createProject(projectInput: { url: $url }) {
      id
    }
  }
`;

export const UPDATE_PROJECT = gql`
  mutation updateCroject($id: Int!, $url: String!) {
    updateProject(projectInput: { id: $id, url: $url }) {
      id
    }
  }
`;

export const REMOVE_PROJECT = gql`
  mutation removeCroject($id: Int!) {
    removeProject(removeProjectInput: { id: $id }) {
      id
    }
  }
`;

export const PROJECT_CREATED = gql`
  subscription created ($userId: Int!) {
    projectCreated (userId: $userId) {
      id
      url
      name
      owner
      stars
      forks
      issues
      createdAt
      updatedAt
    }
  }
`;

export const PROJECT_UPDATED = gql`
  subscription updated ($userId: Int!) {
    projectUpdated (userId: $userId) {
      id
      url
      name
      owner
      stars
      forks
      issues
      createdAt
      updatedAt
    }
  }
`;

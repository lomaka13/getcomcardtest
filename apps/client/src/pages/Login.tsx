import React, { useState } from 'react';
import {
  Button, createStyles,
  CssBaseline,
  FormControl,
  Input,
  InputLabel,
  Paper, Theme,
  Typography
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import { useMutation } from '@apollo/react-hooks';
import { Link } from 'react-router-dom';

import { SIGNUP, LOGIN } from '../graphql';
import SnackBar from '../components/SnackBar';
import { useInput } from '../hooks/input-hook';

const Login = ({ classes, history }) => {
  const isSignUp = history.location.pathname === '/signup';
  const [snackBarSettings, setSnackBarSettings] = useState({
    kind: 'success',
    message: '',
    isOpen: false
  });
  const { value: loginValue, onChange: loginOnChange } = useInput('');
  const { value: passwordValue, onChange: passwordOnChange } = useInput('');
  const mutationAction = isSignUp ? SIGNUP : LOGIN;

  const [login] = useMutation(mutationAction);
  const submitLogin = async event => {
    event.preventDefault();
    const payload = {
      email: loginValue,
      password: passwordValue
    };

    try {
      const { data } = await login({ variables: payload });
      const action = isSignUp ? 'signup' : 'login';

      localStorage.setItem('token', data[action].jwt);
      history.push('/');
    } catch (e) {
      const message = e.graphQLErrors.map(error => {
        if (typeof error.message === 'string') {
          return error.message;
        }
        return error.message.message.map(m => Object.entries(m.constraints).map(
          ([key, value]) => value
        )).join(', ');
      }).join('; ');
      setSnackBarSettings({
        kind: 'error',
        isOpen: true,
        message
      });
    }
  };


  return (
    <div className={classes.wrapper}>
      {
        snackBarSettings.isOpen
        &&
        <SnackBar
          open={snackBarSettings.isOpen}
          kind={snackBarSettings.kind}
          message={snackBarSettings.message}
          handleClose={() => setSnackBarSettings({
            ...snackBarSettings,
            isOpen: false
          })}
        />
      }
      <main className={classes.main}>
        <CssBaseline/>
        <Paper className={classes.paper}>
          <Typography component="h1" variant="h5">
            ComCard
          </Typography>
          <form className={classes.form}
                onSubmit={submitLogin}
          >
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Login</InputLabel>
              <Input
                id="email"
                name="email"
                autoComplete="email"
                autoFocus value={loginValue}
                onChange={loginOnChange}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={passwordValue}
                onChange={passwordOnChange}
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={!(passwordValue && loginValue)}
              className={classes.submit}
            >
              {isSignUp ? 'Sign up' : 'Login'}
            </Button>
            <Link
              to={isSignUp ? '/login' : '/signup'}
              className={classes.anotherAction}
            >
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="secondary"
                className={classes.submit}
              >
                {!isSignUp ? 'Sign up' : 'Login'}
              </Button>
            </Link>
          </form>
        </Paper>
      </main>
    </div>
  );
};

const styles = ({ breakpoints, palette, spacing }: Theme) => createStyles({
  wrapper: {
    backgroundColor: palette.primary.main,
    height: '100vh'
  },
  main: {
    display: 'block',
    [breakpoints.up(400 + spacing() * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderTop: '6px solid salmon',
    padding: `${spacing() * 2}px ${spacing() * 3}px ${spacing() * 3}px`
  },
  avatar: {
    margin: spacing(),
    backgroundColor: palette.primary.main
  },
  form: {
    width: '100%',
    marginTop: spacing()
  },
  submit: {
    marginTop: spacing() * 3
  },
  anotherAction: {
    textDecoration: 'none'
  }
});

export default withStyles(styles)(Login);

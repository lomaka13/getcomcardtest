import React, { useState } from 'react';
import { createStyles, CssBaseline, Theme } from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import { User } from '../helpers';
import SnackBar from '../components/SnackBar';
import Projects from '../components/Projects';
import { User as UserGraphql } from '@getcomcardtest/grapgql-schema';

const Home = ({ history, classes }) => {
  const user: UserGraphql = User(history);

  const [snackBarSettings, setSnackBarSettings] = useState({
    kind: 'success',
    message: '',
    isOpen: false
  });


  return (
    <div className={classes.wrapper}>
      {
        snackBarSettings.isOpen
        &&
        <SnackBar
          open={snackBarSettings.isOpen}
          kind={snackBarSettings.kind}
          message={snackBarSettings.message}
          handleClose={() => setSnackBarSettings({
            ...snackBarSettings,
            isOpen: false
          })}
        />
      }
      <main className={classes.main}>
        <CssBaseline/>
        <Projects userId={user.id}/>
      </main>
    </div>
  );
};

const styles = ({ breakpoints, palette, spacing }: Theme) => createStyles({
  wrapper: {
    backgroundColor: palette.primary.main,
    height: '100vh'
  },
  main: {
    display: 'block',
    [breakpoints.up(400 + spacing() * 3 * 2)]: {
      width: 1024,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderTop: '6px solid salmon',
    padding: `${spacing() * 2}px ${spacing() * 3}px ${spacing() * 3}px`
  }
});


export default withStyles(styles)(Home);

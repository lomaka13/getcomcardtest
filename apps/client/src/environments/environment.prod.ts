export const environment = {
  production: true,
  API_URL: 'https://api.comcard.tk/graphql',
  SOCKET_URL: 'wss://api.comcard.tk/graphql'
};

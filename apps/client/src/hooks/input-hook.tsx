import { useState } from 'react';

export const useInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  return {
    reset: () => setValue(''),
    value,
    onChange: e => setValue(e.target.value)
  };
};

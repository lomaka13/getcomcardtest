import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';
import { red, blue, green, amber } from '@material-ui/core/colors';
import { SnackbarContent, IconButton, Snackbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon
};

const useStyles = makeStyles(() => ({
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: red[700]
  },
  info: {
    backgroundColor: blue[500]
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: '10px'
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  }
}));

const MySnackbarContentWrapper = props => {
  const classes = useStyles({});
  const { className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)}/>
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon className={classes.icon}/>
        </IconButton>
      ]}
      {...other}
    />
  );
};

MySnackbarContentWrapper.propTypes = {
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired
};

/**
 *
 * @param {String} kind
 *  is one of "success", "error", "warning", "info"
 *
 * @param {String} message
 * @param {Boolean} open
 *  true || false is a state of snackbar
 * @param {Function} handleClose
 *  handles snackbar state
 */

const SnackBar = ({
                    open = false,
                    kind,
                    message,
                    handleClose,
                    duration,
                    vertical,
                    horizontal
                  }) => (
  <div>
    <Snackbar
      anchorOrigin={{
        vertical: vertical || 'top',
        horizontal: horizontal || 'center'
      }}
      open={open}
      autoHideDuration={duration || 5000}
      onClose={handleClose}
    >
      <MySnackbarContentWrapper
        onClose={handleClose}
        variant={kind}
        message={message}
      />
    </Snackbar>
  </div>
);

SnackBar.propTypes = {
  kind: PropTypes.string,
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  duration: PropTypes.number,
  vertical: PropTypes.string,
  horizontal: PropTypes.string
};

export default SnackBar;

import React, { useEffect, useState } from 'react';
import MaterialTable, { Column } from 'material-table';
import { useMutation, useSubscription, useQuery } from '@apollo/react-hooks';
import { Project } from '@getcomcardtest/grapgql-schema';
import reject from 'lodash/reject';
import uniq from 'lodash/uniqBy';
import {
  USER_PROJECTS,
  PROJECT_CREATED,
  CREATE_PROJECT,
  UPDATE_PROJECT,
  REMOVE_PROJECT, PROJECT_UPDATED
} from '../../graphql';

interface TableState {
  columns: Array<Column<Project>>;
  data: Project[];
}

const Projects = props => {
  const { data } = useQuery(USER_PROJECTS, {
    variables: { id: props.userId },
    skip: !props.userId
  });
  const [createProject] = useMutation(CREATE_PROJECT);
  const [updateProject] = useMutation(UPDATE_PROJECT);
  const [removeProject] = useMutation(REMOVE_PROJECT);

  const { data: onCreateProject } = useSubscription(PROJECT_CREATED, {
    variables: { userId: props.userId }
  });

  const { data: onUpdateProject } = useSubscription(PROJECT_UPDATED, {
    variables: { userId: props.userId }
  });

  const [state, setState] = useState<TableState>({
    columns: [
      { title: 'Url', field: 'url' },
      { title: 'Owner', field: 'owner', editable: 'never' },
      { title: 'Name', field: 'name', editable: 'never' },
      { title: 'Stars', field: 'stars', editable: 'never' },
      { title: 'Forks', field: 'forks', editable: 'never' },
      { title: 'Issues', field: 'issues', editable: 'never' },
      { title: 'Date create', field: 'createdAt', editable: 'never' }
    ],
    data: []
  });

  useEffect(() => {
    if (data) {
      setState({
        columns: state.columns,
        data: uniq([
          ...state.data,
          ...data.user.projects
        ], 'id')
      });
    }
  // eslint-disable-next-line
  }, [data]);

  useEffect(() => {
    if (onCreateProject) {
      setState({
        columns: state.columns,
        data: uniq([
          ...state.data,
          onCreateProject.projectCreated
        ], 'id')
      });
    }
  // eslint-disable-next-line
  }, [onCreateProject]);

  useEffect(() => {
    if (onUpdateProject) {
      setState({
        columns: state.columns,
        data: uniq([
          ...state.data,
          onUpdateProject.projectUpdated
        ], 'id')
      });
    }
  // eslint-disable-next-line
  }, [onUpdateProject]);

  return (
    <MaterialTable
      title="Stored github projects"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: async newData => {
          await createProject({
            variables: { url: newData.url }
          });
        },
        onRowUpdate: async (newData, oldData) => {
          await updateProject({
            variables: { url: newData.url, id: oldData.id }
          });
        },
        onRowDelete: async oldData => {
          await removeProject({
            variables: { id: oldData.id }
          });
          setState({
            columns: state.columns,
            data: reject(state.data, row => row.id === oldData.id)
          });
        }
      }}
    />
  );
};

Projects.defaultProps = {
  projects: [] as Project[]
};

export default Projects;

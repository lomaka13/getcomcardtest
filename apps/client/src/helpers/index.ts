import KJUR from 'jsrsasign';

export const getToken = () => localStorage.getItem('token');

export const isUserAlreadyLoggedIn = history => {
  const token = getToken();
  if (!token) {
    return history.replace({ pathname: '/login' });
  }
  return token;
};

export const User = history => {
  const token = isUserAlreadyLoggedIn(history);
  const { payloadObj } = KJUR.jws.JWS.parse(token);
  if (payloadObj.exp >= new Date().valueOf()) {
    localStorage.removeItem(token);
    return history.replace({ pathname: '/login' });
  }
  return payloadObj;
};

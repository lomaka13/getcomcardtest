import { IsUrl } from 'class-validator';
import { ProjectInput } from '@getcomcardtest/grapgql-schema';

export class ProjectDto extends ProjectInput {
  @IsUrl()
  readonly url: string;
}

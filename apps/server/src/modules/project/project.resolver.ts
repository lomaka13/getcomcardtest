import { Args, Mutation, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';

import { GqlAuthGuard } from '../../helpers/guards/graphql.guard';
import { UseGuards } from '@nestjs/common';
import { PrismaService } from '../../providers/prisma/prisma.service';
import { RemoveProjectInput, SubsProject } from '@getcomcardtest/grapgql-schema';
import { GqlUser } from '../../helpers/decorators/decorators';

import { ProjectDto } from './project.dto';
import { ProjectService } from './project.service';

const pubSub = new PubSub();

@Resolver('Project')
export class ProjectResolver {
  constructor(
    private readonly prisma: PrismaService,
    private readonly service: ProjectService
  ) {
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  async createProject(
    @Args('projectInput') { url }: ProjectDto,
    @GqlUser() user
  ) {
    const { id } = user;
    const created = await this.prisma.client.createProject({ url });
    await this.prisma.client.updateUser({
      where: { id },
      data: {
        projects: {
          connect: {
            id: created.id
          }
        }
      }
    });
    this.serveProject(created.id, created.url, 'projectCreated', id);
    return created;
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  async updateProject(
    @Args('projectInput') { id, url }: ProjectDto,
    @GqlUser() user
  ) {
    const updated = await this.prisma.client.updateProject({
      data: { url },
      where: { id }
    });
    this.serveProject(updated.id, updated.url, 'projectUpdated', user.id);
    return updated;
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  async removeProject(
    @Args('removeProjectInput') { id }: RemoveProjectInput
  ) {
    return this.prisma.client.deleteProject({ id });
  }

  @Subscription('projectCreated', {
    filter: (payload, variables) => {
      return payload.projectCreated.userId === variables.userId;
    }
  })
  async projectCreated() {
    return pubSub.asyncIterator<SubsProject>('projectCreated');
  }

  @Subscription('projectUpdated', {
    filter: (payload, variables) => {
      return payload.projectUpdated.userId === variables.userId;
    }
  })
  async projectUpdated() {
    return pubSub.asyncIterator<SubsProject>('projectUpdated');
  }

  async serveProject(id: number, url: String, action: String, userId: number) {
    const {
      owner,
      name,
      stargazers_count: stars,
      forks_count: forks,
      open_issues_count: issues
    }: any = await this.service.fetchGithub(url);

    const updated = await this.prisma.client.updateProject({
      where: { id },
      data: {
        owner: owner.login,
        name,
        stars,
        forks,
        issues
      }
    });
    return pubSub.publish(action as string, {
      [action as string]: {
        ...updated,
        userId
      }
    });
  }
}

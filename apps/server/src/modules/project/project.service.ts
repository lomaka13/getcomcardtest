import { HttpService, Injectable } from '@nestjs/common';
import { parse } from "url";

@Injectable()
export class ProjectService {
  constructor (
    private readonly http: HttpService,
  ){}

  async fetchGithub (url) {
    const parsedUrl = parse(url);
    const [, owner, repo] = parsedUrl.pathname.split('/');
    const response = await this.http.get(`https://api.github.com/repos/${owner}/${repo}`).toPromise();
    return response.data;
  }
}

import { Module, HttpModule } from '@nestjs/common';
import { ProjectResolver } from './project.resolver';
import { DateScalar } from '../../helpers/scalars/date.scalar';
import { PrismaModule } from '../../providers/prisma/prisma.module';
import { JwtModule } from '@nestjs/jwt';
import { ProjectService } from './project.service';
import jwtConfig from '../../helpers/custom_configs/jwt.config';

@Module({
  imports: [
    PrismaModule,
    JwtModule.registerAsync({
      useClass: jwtConfig
    }),
    HttpModule,
  ],
  providers: [ProjectResolver, DateScalar, ProjectService]
})
export class ProjectModule {}

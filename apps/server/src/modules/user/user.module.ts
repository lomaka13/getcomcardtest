import { Module } from '@nestjs/common';
import { UserResolver } from './user.resolver';
import { PrismaModule } from '../../providers/prisma/prisma.module';
import { JwtModule } from '@nestjs/jwt';
import jwtConfig from '../../helpers/custom_configs/jwt.config';

@Module({
  imports: [
    PrismaModule,
    JwtModule.registerAsync({
      useClass: jwtConfig
    }),
  ],
  providers: [UserResolver]
})
export class UserModule {}

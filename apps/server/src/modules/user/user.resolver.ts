import { Resolver, Query, Args, ResolveProperty, Parent } from '@nestjs/graphql';
import { PrismaService } from '../../providers/prisma/prisma.service';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '../../helpers/guards/graphql.guard';

@Resolver('User')
export class UserResolver {
  constructor(
    private readonly prisma: PrismaService
  ) {}

  @Query()
  @UseGuards(GqlAuthGuard)
  async user(@Args('id') id: number) {
    return await this.prisma.client.user({ id });
  }

  @ResolveProperty()
  async projects(@Parent() user) {
    const { id } = user;
    return await await this.prisma.client.user({ id }).projects();
  }
}

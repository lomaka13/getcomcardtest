import { IsEmail, MinLength } from 'class-validator';
import { LoginInput } from '@getcomcardtest/grapgql-schema';

export class LoginInputDto extends LoginInput {
  @IsEmail()
  readonly email: string;

  @MinLength(6)
  readonly password: string;
}

import * as bcryptjs from 'bcryptjs';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from '../../providers/prisma/prisma.service';
import { LoginInputDto } from './auth.dto';

@Resolver('Auth')
export class AuthResolver {
  constructor(
    private readonly jwt: JwtService,
    private readonly prisma: PrismaService,
  ) {}

  @Mutation()
  async login(
    @Args('loginInput') { email, password }: LoginInputDto
  ) {
    const [user] = await this.prisma.client.users({ where: { email } });
    if (!user) {
      throw Error('Email or password incorrect');
    }

    const valid = await bcryptjs.compare(password, user.password);
    if (!valid) {
      throw Error('Email or password incorrect');
    }

    delete user.password;
    const jwt = this.jwt.sign(user);

    return { jwt };
  }

  @Mutation()
  async signup(
    @Args('signUpInput') signUpInputDto: LoginInputDto
  ) {
    const emailExists = await this.prisma.client.$exists.user({
      email: signUpInputDto.email,
    });
    if (emailExists) {
      throw Error('Email is already in use');
    }
    const password = await bcryptjs.hash(signUpInputDto.password, 10);

    const user = await this.prisma.client.createUser({ ...signUpInputDto, password });

    delete user.password;
    const jwt = this.jwt.sign(user);
    return { jwt };
  }
}

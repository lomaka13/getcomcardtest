import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PrismaModule } from "../../providers/prisma/prisma.module";
import { AuthResolver } from './auth.resolver';
import jwtConfig from '../../helpers/custom_configs/jwt.config';;

@Module({
  imports: [
    PrismaModule,
    JwtModule.registerAsync({
      useClass: jwtConfig
    })
  ],
  providers: [AuthResolver],
})
export class AuthModule {}

import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { join } from 'path';
import * as jwt from 'jsonwebtoken';
import config from '../../environments';

@Injectable()
export class GraphqlOptions implements GqlOptionsFactory {
  createGqlOptions(): Promise<GqlModuleOptions> | GqlModuleOptions {
    return {
      context: ({ req, res }) => ({ req, res }),
      typePaths: ['./apps/server/src/**/*.graphql'],
      installSubscriptionHandlers: true,
      resolverValidationOptions: {
        requireResolversForResolveType: false
      },
      definitions: {
        path: join(process.cwd(), 'tools/schematics', 'graphql.ts'),
        outputAs: 'class'
      },
      debug: true,
      introspection: true,
      playground: !config.production,
      cors: true,
      subscriptions: {
        onConnect: async connParams => {
          const token = connParams['authToken'];
          if (token) {
            jwt.verify(token.replace('Bearer ', ''), config.jwt.secret);
          } else {
            throw new UnauthorizedException('Unauthorized', 'User not authorized to connect.');
          }
        }
      }
    };
  }
}

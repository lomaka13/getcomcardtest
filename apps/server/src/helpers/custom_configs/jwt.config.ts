import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';
import config from '../../environments';;

export default class implements JwtOptionsFactory {
  createJwtOptions(): JwtModuleOptions {
    return {
      secret: config.jwt.secret,
      signOptions: {
        expiresIn: config.jwt.expires,
      },
    };
  }
}

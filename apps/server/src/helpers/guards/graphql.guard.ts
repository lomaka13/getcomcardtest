import { ExecutionContext, Injectable, CanActivate, UnauthorizedException/*, BadRequestException*/ } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { PrismaService } from '../../providers/prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class GqlAuthGuard implements CanActivate {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwt: JwtService
  ) {}
  async canActivate(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();
    if (!req) {
      return ctx;
    }
    await this.checkToken(req);
    // await this.checkSign(req);
    return req;
  }

  async checkToken (req) {
    const token: string = req.headers.authorization;
    if (!token) {
      throw new UnauthorizedException();
    }
    const { id } = await this.jwt.verifyAsync(token.replace('Bearer ', ''));
    const user = await this.prisma.client.user({ id });
    if (!user) {
      throw Error('Authenticate validation error');
    }
    // eslint-disable-next-line
    req.user = user;
  }

  // async checkSign (req) {
  //   const sign: string = req.headers.sign;
  //   if (!sign) {
  //     throw new BadRequestException();
  //   }
  //   todo verify
  //   return true;
  // }
}

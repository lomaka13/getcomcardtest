import { createParamDecorator } from '@nestjs/common';
import { Response } from 'express';

export const ResGql = createParamDecorator(
  (data, [, , ctx]): Response => ctx.res,
);

export const GqlToken = createParamDecorator(
  (data, [,, ctx]): String =>
    ctx.req.headers && ctx.req.headers.authorization && ctx.req.headers.authorization.replace('Bearer ', ''),
);

export const GqlUser = createParamDecorator(
  (data, [,, ctx]): String => ctx.req.user
);

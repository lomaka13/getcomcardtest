import { environment as local } from './environment';
import { environment as prod } from './environment.prod';

export default process.env.NODE_ENV === 'production' ? prod : local;

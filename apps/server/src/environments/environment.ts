export const environment = {
  production: false,
  server: {
    port: 3000
  },
  jwt: {
    secret: 'local',
    expires: 3600,
  },
};

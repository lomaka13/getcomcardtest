export const environment = {
  production: true,
  server: {
    port: 3000
  },
  jwt: {
    secret: '2d33fb93-5f5a-40e9-94f1-658f396b92d2',
    expires: 3600,
  },
};

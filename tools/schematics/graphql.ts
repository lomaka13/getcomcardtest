
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class LoginInput {
    email: string;
    password: string;
}

export class ProjectInput {
    id?: number;
    url: string;
}

export class RemoveProjectInput {
    id: number;
}

export class AuthPayload {
    jwt: string;
}

export abstract class IMutation {
    abstract signup(signUpInput?: LoginInput): AuthPayload | Promise<AuthPayload>;

    abstract login(loginInput?: LoginInput): AuthPayload | Promise<AuthPayload>;

    abstract createProject(projectInput?: ProjectInput): Project | Promise<Project>;

    abstract updateProject(projectInput?: ProjectInput): Project | Promise<Project>;

    abstract removeProject(removeProjectInput?: RemoveProjectInput): Project | Promise<Project>;
}

export class Project {
    id: number;
    url: string;
    owner?: string;
    name?: string;
    stars?: number;
    forks?: number;
    issues?: number;
    createdAt: Date;
    updatedAt: Date;
}

export abstract class IQuery {
    abstract project(id: number): Project | Promise<Project>;

    abstract user(id: number): User | Promise<User>;
}

export abstract class ISubscription {
    abstract projectCreated(userId: number): Project | Promise<Project>;

    abstract projectUpdated(userId: number): Project | Promise<Project>;
}

export class SubsProject {
    id?: number;
    url?: string;
    owner?: string;
    name?: string;
    stars?: number;
    forks?: number;
    issues?: number;
    createdAt?: Date;
    updatedAt?: Date;
}

export class User {
    id: number;
    email: string;
    projects?: Project[];
}
